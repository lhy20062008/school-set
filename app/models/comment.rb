class Comment < ApplicationRecord
  belongs_to :target, polymorphic: true, optional: true
  has_many :sub_comments, as: :target, class_name: 'Comment'
  belongs_to :user

  delegate :name, :avatar, to: :user, prefix: :user, allow_nil: true

  def destroy_with_sub_comments!
    sub_comments.map(&:destroy)
    destroy
  end
end


# revert 1

# revert 2

# revert 3