class Comments < Api
  resources :comments do
    desc 'Get Comments'
    params do 
      requires :target_id, type: Integer, default: 1
      optional :target_type, type: String, values: %w(School Comment)
      optional :page, type: Integer, default: 1
      optional :per_page, type: Integer, default: 10
    end
    get do
      opts = {target_id_eq: params[:target_id], target_type: params[:target_type] || 'School'}
      comments = Comment.ransack(opts).result.includes(:user).order('id desc').page(params[:page] || 1).per(params[:per_page] || 10)
      response_code(:ok)
      response_data(comments: Entities::Comment.represent(comments), pagination: pagination(comments))
    end

    desc 'Create Comment'
    params do
      requires :target_id, type: Integer
      requires :target_type, type: String, values: %w(School Comment)
      requires :content, type: String
    end
    post do
      target = params[:target_type].constantize.find_by_id(params[:target_id])
      if target.present?
        comment = current_user.comments.create(target: target, content: params[:content])
        response_code(:ok)
        response_data(comment: Entities::Comment.represent(comment))
      else
        response_code(:not_found)
      end
    end

    desc 'Delete Comment'
    delete ':id' do 
      comment = Comment.find_by_id(params[:id])
      response_code(:not_found) and return unless comment.present?
      if comment.user_id == current_user.id
        comment.destroy_with_sub_comments!
        response_code(:ok)
      else
        response_code(:fail, '只能删除自己的评论')
      end
    end
  end
end
