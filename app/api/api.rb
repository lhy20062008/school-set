require 'grape-swagger'
class Api < Grape::API
  format :json
  helpers Authenticate
  helpers ResponseHelper
  before do
    authenticate unless [/swagger_doc/].any?{|x| request.path =~ x}
  end

  mount Comments

  add_swagger_documentation
end
