module Entities
  class School < Grape::Entity
    expose :id, :name, :created_at
  end
end
