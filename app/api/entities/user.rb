module Entities
  class User < Grape::Entity
    expose :id, :name, :created_at
  end
end
