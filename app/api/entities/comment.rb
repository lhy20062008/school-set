module Entities
  class Comment < Grape::Entity
    expose :id, :content, :created_at, :user_id, :user_name, :user_avatar
    expose :sub_comments do |comment|
      comment.sub_comments.includes(:user).order('id desc').limit(3).map do |sub_comment|
        Entities::Comment.represent(sub_comment, only: [:id, :content, :user_avatar, :user_name, :user_id]).as_json
      end
    end
  end
end
