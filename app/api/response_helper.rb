module ResponseHelper
  def response_code code = nil, message = nil
    @response_codes ||= {
      ok: 'OK',
      fail: 'Fail',
      unauthorized: 'Unauthorized',
      not_found: 'Not Found',
      internal_server_error: 'Internal Server Error'
    }
    present :code, code
    present :message, message || @response_codes[code]
  end

  def response_data data = {}
    present :data, data, with: Grape::Presenters::Presenter
  end

  def pagination items
    {
      current_page: items.current_page.to_i,
      next_page: items.next_page,
      prev_page: items.prev_page,
      per_page: items.limit_value,
      total_pages: items.total_pages,
      total_count: items.total_count
    }
  end

  def full_messages(record = nil)
    record.errors.full_messages.join(", ")
  end
end
