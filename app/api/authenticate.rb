module Authenticate
  def authenticate
    @current_user = User.first
  end

  def current_user
    @current_user
  end

  def unauthorized
    error!('Unauthorized', 401)
  end
end
