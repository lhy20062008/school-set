10.times do |i|
  User.create(name: "用户#{i + 1}", avatar: '')
end

10.times do |i|
  school = School.create(name: "学校#{i + 1}")
  rand(20).times do |j|
    comment = Comment.create(target: school, content: "根评论#{j + 1}", user_id: User.ids.sample(1)[0])
    rand(5).times do |k|
      Comment.create(target: comment, content: "评论回复#{k + 1}", user_id: User.ids.sample(1)[0])
    end
  end
end

