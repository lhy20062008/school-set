class CreateComments < ActiveRecord::Migration[6.1]
  def change
    create_table :comments do |t|
      t.integer :user_id
      t.string :content
      t.integer :target_id
      t.string :target_type
      
      t.timestamps
    end
  end
end
