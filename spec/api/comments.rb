require 'rails_helper'

describe Comments, type: :request do
  before do
    @school1 = create(:school1)
    @school2 = create(:school2)
    @user1 = create(:user1)
    @user2 = create(:user2)
    @comment1 = create(:base_comment, user: @user1, target: @school1)
    @comment2 = create(:base_comment, user: @user2, target: @school2)
  end
  context 'GET /comments' do
    it 'get comments' do
      get '/comments', params: {target_id: @school1.id}
      result = JSON.parse(response.body)
      expect(result['code']).to eq('ok')
      expect(result['data']['comments'].count).to eq(1)
    end

    it 'get page 2 comments' do 
      get '/comments', params: {target_id: @school1.id, page: 2}
      result = JSON.parse(response.body)
      expect(result['code']).to eq('ok')
      expect(result['data']['comments'].count).to eq(0)
    end
  end

  context 'POST /comments' do 
    it 'POST root Comment' do 
      post '/comments', params: {target_type: 'School', target_id: @school1.id, content: 'content'}
      result = JSON.parse(response.body)
      expect(result['code']).to eq('ok')
      expect(Comment.where(target: @school1).count).to eq(2)
    end

    it 'POST sub Comment' do 
      post '/comments', params: {target_type: 'Comment', target_id: @comment1.id, content: 'sub content'}
      result = JSON.parse(response.body)
      expect(result['code']).to eq('ok')
      expect(Comment.where(target: @comment1).count).to eq(1)
    end
  end

  context 'DELETE comment' do
    it 'DELETE comment which comment not exists' do
      delete "/comments/#{@comment2.id + 1}"
      result = JSON.parse(response.body)
      expect(result['code']).to eq('not_found')
    end

    it 'DELETE comment2' do
      delete "/comments/#{@comment2.id}"
      result = JSON.parse(response.body)
      expect(result['code']).to eq('fail')
    end

    it 'DELETE comment1' do
      delete "/comments/#{@comment1.id}"
      result = JSON.parse(response.body)
      expect(result['code']).to eq('ok')
    end
  end
end
