FactoryBot.define do
  factory :base_comment, class: Comment do
    content { 'comment content' }
  end
end
