FactoryBot.define do
  factory :school1, class: School do
    name { 'school1' }
  end

  factory :school2, class: School do 
    name { 'school2' }
  end
end
