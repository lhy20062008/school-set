FactoryBot.define do
  factory :user1, class: User do
    name { 'user1' }
  end

  factory :user2, class: User do 
    name { 'user2' }
  end
end
