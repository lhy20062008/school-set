Rails.application.routes.draw do
  mount Api => '/'
  mount GrapeSwaggerRails::Engine => '/api_doc'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
